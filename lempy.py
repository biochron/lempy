"""
:author: Francis C. Motta
:email: motta<at>math<dot>duke<dot>edu
:created: 04/03/2017
:updated: 12/04/2018
:copyright: (c) 2017, Francis C. Motta

This Python module implements the Local Edge Machine (https://doi.org/10.1186/s13059-016-1076-z) 
used to infer functional interactions of variables from time series data.

Contains classes and routines for storing and analyzing time series expression data and computing
posterior probabilities on local variable regulation model spaces. 
"""

from loss_functions import *
from prior_distributions import *
from rhs_models import *
import utilities as utls
from configobj import ConfigObj
import random as rng
import mpmath as mp
import numpy as np
import pandas as pd
import scipy.optimize as sci
import time as tmr
import datetime
import inspect
import sys
import os

VERSION = "1.0"

class localModel(object):
    """
    class describing a functional interaction between a pair of genes in a gene regulatory network
    :attribute rhs: function defining the right-hand side of the ODE describing the rate of change of tar
    :attribute param_bounds: list of pairs, [(min, max)], giving allowable rhs param ranges
    :attribute loss: function defining the goodness of fit of the predicted target series to the working target data
    :attribute time: length-T list of original times of data points
    :attribute time_working: length-T list of current, working times of data points
    :attribute tar/reg_name: strings encoding names of target and regulator
    :attribute tar/reg_series: length-T lists of original time series data
    :attribute tar/reg_working: length-T lists of current, working time series data used for calculations
    :attribute minimizer params: dictionary of optional scipy.basinhopping arguments
    :attribute minima_rhs_params: the set of local minima of the rhs parameters found during global optimization
    :attribute optimal_rhs_params: the set of parameters of rhs which minimizes the loss function
    :attribute optimal_rhs_loss: the value of the loss function at optimal_rhs_params
    :attribute optimal_tar_pred: the local prediction of the time series using optimal_rhs_params
    :attribute beta: inverse temperature float used in calculation of posterior, default = 1
    :attribute posterior: estimate of unnormalized posterior probability from Gibbs posterior principle
    :attribute hessian_at_optimum: the Hessian of the loss function at optimal rhs parameters
    :attribute hessian_eigs: eigenvalues of the Hessian of the loss function at optimal rhs parameters
    :attribute null_loss: minimial loss of the parameterizes null model of regulation
    :attribute opt_time: float indicating time taken to perform optimization
    :attribute opt_time: float indicating time taken to perform optimization
    :attribute optimized_on_working: boolean flag indicating if optimal_rhs_params were determined for tar/reg_working
    :method optimize_rhs_params(): perform global optimization of rhs params to minimize loss function
    :method compute_null_loss(): perform global optimization of rhs params of null model to minimize loss function
    :method pld(beta): returns an estimate of the (unnormalized) posterior probability of the model of regulation,
                       conditioned on the data, using the Laplace approximation formula with inverse temperature, beta
                       using Gibbs posterior principle
    """

    def __init__(self, rhs, param_bounds, loss,
                 time_series, tar_name, tar_series, reg_name, reg_series,
                 minimizer_params):
        self.rhs = rhs
        self.param_bounds = param_bounds()
        self.loss = loss
        self.time = time_series
        self.time_working = time_series
        self.tar_name = tar_name
        self.tar_series = tar_series
        self.tar_working = tar_series
        self.reg_name = reg_name
        self.reg_series = reg_series
        self.reg_working = reg_series
        self.minimizer_params = minimizer_params
        self.minima_rhs_params = np.array([])  # run optimize_rhs_params to update value based on *_working attributes
        self.minima_rhs_loss = np.array([])  # run optimize_rhs_params to update value based on *_working attributes
        self.optimal_rhs_params = None  # run optimize_rhs_params to update value based on *_working attributes
        self.optimal_rhs_loss = None  # run optimize_rhs_params to update value based on *_working attributes
        self.optimal_tar_pred = None  # run optimize_rhs_params to update value based on *_working attributes
        self.params_on_boundary = None  # run optimize_rhs_params to update value based on *_working attributes
        self.hessian_at_optimum = None  # run optimize_rhs_params to update value based on *_working attributes
        self.null_loss = None  # run compute_null_loss to update value based on model of no regulation
        self.opt_time = None  # run optimize_rhs_params to update value
        self.posterior = None  # run pld to update value based on *_working attributes
        self.hessian_eigs = None  # run pld to update value based on *_working attributes
        self.hessian_det = None  # run pld to update value based on *_working attributes
        self.beta = None  # run pld to specify value
        self.pld_time = None  # run pld to update value
        self.optimized_on_working = False

    def compute_null_loss(self, seed=None):
        """
        compute and return the loss for the null model of regulation
        """
        if seed is not None:
            rng.seed(seed)

        # establish allowable parameter region and set optimization arguments
        minimizer_kwargs = {'method': "L-BFGS-B",
                            'bounds': null_bounds(),
                            'args': (self.time_working, self.reg_working, self.tar_working, null)}
        # generate an initial parameter guess
        x0 = [rng.uniform(p[0], p[1]) for p in null_bounds()]
        # run parameter optimization
        results = sci.basinhopping(self.loss,
                                   np.array(x0),
                                   minimizer_kwargs=minimizer_kwargs,
                                   **self.minimizer_params)
        self.null_loss = results.fun
        return results.x, results.fun

    def optimize_rhs_params(self, seed=None):
        """
        compute estimate of global optimal choice of parameters of self.rhs, i.e. minimum of loss(x, model)
        and update object attributes at optimum
        """
        begin_time = tmr.process_time()
        if seed is not None:
            rng.seed(seed)

        # define the callback function used to record each local minima found during global optimization
        def record_minima(x, f, accept):
            x = np.reshape(x, (1, len(x)))
            if self.minima_rhs_params.size == 0:
                self.minima_rhs_params = x
            else:
                self.minima_rhs_params = np.append(self.minima_rhs_params, x, axis=0)
            self.minima_rhs_loss = np.append(self.minima_rhs_loss, f)
            return None

        # define a partial function that depends on the rhs model and the working expression profiles
        def _loss(*params):
            return self.loss(list(params), self.time_working, self.reg_working, self.tar_working, self.rhs)

        # establish allowable parameter region and set optimization arguments
        minimizer_kwargs = {'method': "L-BFGS-B",
                            'bounds': self.param_bounds,
                            'args': (self.time_working, self.reg_working, self.tar_working, self.rhs)}
        # generate an initial parameter guess
        x0 = [rng.uniform(p[0], p[1]) for p in self.param_bounds]
        # run parameter optimization
        results = sci.basinhopping(self.loss,
                                   np.array(x0),
                                   callback=record_minima,
                                   minimizer_kwargs=minimizer_kwargs,
                                   **self.minimizer_params)
        self.optimal_rhs_params = results.x
        self.optimal_rhs_loss = results.fun
        self.optimal_tar_pred = self.loss(self.optimal_rhs_params,
                                          self.time_working,
                                          self.reg_working,
                                          self.tar_working,
                                          self.rhs,
                                          return_pred=True)
        # identify the number of parameters on the boundary of allowable parameter space
        self.params_on_boundary = sum([1 if self.param_bounds[bound_i][0] == results.x[bound_i] or
                                            self.param_bounds[bound_i][1] == results.x[bound_i] else
                                       0 for bound_i in range(len(self.param_bounds))])
        # estimate the Hessian at the optimal parameter value
        num_params = len(results.x)
        hessian_at_optimum = np.zeros(shape=(num_params, num_params))
        for hess_i in range(0, num_params):
            for hess_j in range(hess_i, num_params):
                hessian_at_optimum[hess_i, hess_j] = mp.diff(_loss,
                                                             tuple(results.x),
                                                             utls.second_deriv_tuple(num_params, hess_i, hess_j))
        self.hessian_at_optimum = hessian_at_optimum
        # set flag to indicate optimization has been run on current working data
        self.optimized_on_working = True
        self.opt_time = tmr.process_time() - begin_time

    def pld(self, beta=1.0, seed=None):
        # computes the unnormalized posterior probability of the regulatory model in light of the observed data
        if not self.optimized_on_working:
            self.optimize_rhs_params(seed=seed)
        begin_time = tmr.process_time()
        self.beta = beta
        d = self.params_on_boundary
        hess = self.hessian_at_optimum
        eigs, _ = np.linalg.eigh(hess, UPLO='U')
        eigs = [1 if eig < 1 else eig for eig in eigs]
        self.hessian_eigs = eigs
        self.hessian_det = np.prod(eigs)
        self.posterior = np.exp(-beta * np.sqrt(self.optimal_rhs_loss)) / \
                         ((2 ** d) * np.sqrt(self.hessian_det))
        self.pld_time = tmr.process_time() - begin_time


if __name__ == '__main__':
    """
    initialize LEMpy run with configuration file, setting defaults as necessary, overwriting configuration file 
    values with optional command line inputs
    == required configuration keys ==
      - data_files: list of full directory and filename(s) of .tsv file(s) containing time series expression data 
                    (NOTE: a list with a single data file must end with a comma)
      - output_dir: full directory in which to save LEMpy output
      - loss: name of the python function defining the loss function used in Gibbs posterior
      - param_bounds: name of the python function which returns the parameter bounds for local models
      - [targets]: section containing a list of target variable names for which to compute posterior distributions 
                   (NOTE: variable name must be followed by '=') 
      - [regulator]: section containing a list of regulator variable names each followed by a list of python functions 
                     specifying the allowed regulatory models 
                     (NOTE: a list with a single allowed model must end with a comma)
    == optional configuration keys ==
      - ground_truth_file: path to the list of regulation edges which are assumed to be true edges
      - prior: name of the python function which returns a dictionary encoding a prior distribution
               on the space of regulatory models
      - normalize: boolean parameter specifying if time series should be normalized (default: True)
      - verbose: boolean parameter specifying if output should print to screen (default: False)
      - inv_temp: hyperparameter >= 0 controlling the strength of the update to the prior given the data (default: 1.0)
      - seed: number specifying the seed for random number generation (default: time.time)
      - [minimizer_params]: section containing optional parameters for scipy.basinhopping
    """
    import argparse
    import prior_distributions as prdist
    from mpi4py import MPI
    from mpi4py.futures import MPICommExecutor
    from functools import partial

    parser = argparse.ArgumentParser(prog='LEMpy v1.0',
                                     description='local inference of variable interactions from time series data')

    # - required command-line argument -
    parser.add_argument('config_file',
                        action='store',
                        help='full path to LEMpy configuration file')
    # - optional command-line arguments -
    parser.add_argument('--loss',
                        nargs=1,
                        help='loss function handle')
    parser.add_argument('--param_bounds',
                        nargs=1,
                        help='param_bounds function handle')
    parser.add_argument('--prior',
                        nargs=1,
                        help='prior function handle')
    parser.add_argument('--normalize',
                        nargs=1,
                        help='normalize time series flag (True/False)')
    parser.add_argument('--output_dir',
                        nargs=1,
                        help='full path to LEMpy output directory')
    parser.add_argument('--ground_truth_file',
                        nargs=1,
                        help='full path to ground truth edge list')
    parser.add_argument('--verbose',
                        nargs=1,
                        help='verbose terminal output flag')
    parser.add_argument('--inv_temp',
                        nargs=1,
                        help='posterior update hyperparameter')
    parser.add_argument('--seed',
                        nargs=1,
                        help='seed for random number generation')
    args = parser.parse_args()

    # load and parse configuration file
    config_file = args.config_file
    config_file_name = os.path.basename(config_file)
    co = ConfigObj(config_file)
    co['config_file'] = config_file

    # overwrite executed/saved configuration file based on optional command-line inputs
    if args.loss:
        co['loss'] = args.loss
    if args.param_bounds:
        co['param_bounds'] = args.param_bounds
    if args.prior:
        co['prior'] = args.prior
    if args.normalize:
        co['normalize'] = args.normalize
    if args.verbose:
        co['verbose'] = args.verbose
    if args.output_dir:
        co['output_dir'] = args.output_dir
    if args.inv_temp:
        co['inv_temp'] = args.inv_temp
    if args.seed:
        co['seed'] = args.seed

    # generate a dictionary of LEMpy defined functions for safe execution of functions specified in config. file
    fxn_dict = dict(inspect.getmembers(sys.modules[__name__], inspect.isfunction))
    # check that the functions specified in the config file exist as python functions in the LEMpy module
    config_fxns = utls.config_file_functions(co)
    invalid_config_fxns = [fxn for fxn in config_fxns if fxn not in fxn_dict.keys()]
    if invalid_config_fxns:
        raise NameError('Specified configuration functions not defined in LEMpy: ' + ', '.join(invalid_config_fxns))

    # load required time series data (targets and regulators) into a dictionary of DataFrames
    tar_names = co['targets'].keys()
    reg_names = co['regulators'].keys()
    col_names = list(set(['time'] + tar_names + reg_names))
    num_reps = len(co['data_files'])
    series_df_dict = {}
    for rep_idx in range(num_reps):
        temp_df = pd.read_csv(co['data_files'][rep_idx], delimiter='\t', comment='#', index_col=0).transpose()
        temp_df['time'] = pd.to_numeric(temp_df.index)
        temp_df.reset_index(inplace=True)
        series_df_dict[rep_idx] = temp_df.loc[:, col_names]

    # generate a list of regulation model names
    reg_model_names = ['%s(%s)' % (rhs, reg_name)
                       for reg_name in reg_names
                       for rhs in co['regulators'][reg_name]]

    # point to the parameter bounds function to be used
    param_bounds = fxn_dict[co['param_bounds']]

    # point to the loss function to be used
    loss = fxn_dict[co['loss']]

    # generate minimizer parameter dictionary by converting configuration strings to correct types and mapping
    # function handles to their python defined functions and set defaults
    minimizer_params = co.get('minimizer_params')
    if minimizer_params:
        minimizer_params.update({key: utls.str_to_type(val, fxn_dict) for key, val in minimizer_params.items()})
    else:
        minimizer_params = {}
    if not minimizer_params.get('niter'):
        minimizer_params['niter'] = 50
    if not minimizer_params.get('accept_test'):
        minimizer_params['accept_test'] = utls.bounded_accept_test
    if not minimizer_params.get('take_step'):
        minimizer_params['take_step'] = utls.bounded_take_step()

    # set inverse temperature
    if not co.get('inv_temp'):
        inv_temp = 1.0
        co['inv_temp'] = inv_temp
    else:
        inv_temp = float(co.get('inv_temp'))

    # set rng seed if necessary
    if co.get('seed'):
        rng_seed = co.get('seed')
    else:
        rng_seed = tmr.time()

    # normalize time series unless otherwise specified
    if not co.get('normalize'):
        co['normalize'] = True
    if utls.str_to_bool(''.join(co['normalize'])):
        for rep_i in range(num_reps):
            # shift start time to 0
            min_time = min(series_df_dict[rep_i]['time'])
            shifted_time = series_df_dict[rep_i]['time'].apply(lambda x: x - min_time)
            series_df_dict[rep_i].loc[:, 'time'] = shifted_time
            for col_name in series_df_dict[rep_i]:
                max_exp = max(series_df_dict[rep_i][col_name])
                # normalize each series by maximum expression
                norm_col = series_df_dict[rep_i][col_name].apply(lambda x: 100 * x / max_exp)
                series_df_dict[rep_i].loc[:, col_name] = norm_col


    """
    MPI parallelized implementation of parameter optimization:
    for each replicate and for each target we construct regulatory model dictionaries whose string keys encode
    the regulation models and whose values are equal to the corresponding localModel objects,
    e.g. lem_model_dict[1]['ACE2'] is the dictionary of local regulatory models (localModel object) for 'ACE2' with 
    data coming from the second replicate time series file.
    """

    task_list = []
    # loop over replicates, targets, and regulatory models and construct lem model dictionary
    for rep_idx in range(num_reps):
        for tar_name in tar_names:
            for reg_name in reg_names:
                for rhs in co['regulators'][reg_name]:
                    reg_model_name = '%s(%s)' % (rhs, reg_name)
                    local_model = localModel(rhs=fxn_dict[rhs],
                                                               param_bounds=param_bounds,
                                                               loss=loss,
                                                               time_series=series_df_dict[rep_idx]['time'].values,
                                                               tar_name=tar_name,
                                                               tar_series=series_df_dict[rep_idx][tar_name].values,
                                                               reg_name=reg_name,
                                                               reg_series=series_df_dict[rep_idx][reg_name].values,
                                                               minimizer_params=minimizer_params)
                    task_list.extend([(rep_idx,tar_name,reg_model_name,local_model)])


    def update_model(beta,seed,ind_target_model_object):
        rep_idx, tar_name, reg_model_name, local_model = ind_target_model_object
        local_model.pld(beta=beta, seed=seed)
        return (rep_idx, tar_name, {reg_model_name: local_model})


    work_function = partial(update_model,inv_temp,rng_seed)


    with MPICommExecutor(MPI.COMM_WORLD, root=0) as executor:
        if executor is not None:
            # distribute and compute models
            output = list(executor.map(work_function, task_list))

            # build model dictionary from output
            lem_model_dict = {}
            for rep_idx, tar_name,model_dict in output:
                lem_model_dict.setdefault(rep_idx,{})
                lem_model_dict[rep_idx].setdefault(tar_name, {})
                lem_model_dict[rep_idx][tar_name].update(model_dict)

            # prior distribution and file output is handled by root process once all tasks are complete
            # create output directories
            if not os.path.exists(''.join(co['output_dir'])):
                os.makedirs(''.join(co['output_dir']))
            if not os.path.exists(''.join(co['output_dir']) + '/targets'):
                os.makedirs(''.join(co['output_dir']) + '/targets')

            # save modified configuration file to output directory
            co.filename = ''.join(co['output_dir']) + '/' + os.path.basename(co.filename)
            co.write()

            # write target files to output directories
            post_dict = {}
            now = datetime.datetime.now()
            for rep_idx in range(num_reps):
                # initialize an all scores data frame to summarize target files
                all_scores_df = pd.DataFrame(columns=['model', 'pld', 'loss', 'norm_loss'])
                # loop over each target localModel object and write output
                for tar_name in tar_names:
                    if rep_idx == 0:
                        # first (perhaps only) replicate so generate prior distributions (default: uniform_prior)
                        if 'prior' in co.keys():
                            prior_dict = fxn_dict[co['prior']](reg_model_names)
                        else:
                            prior_dict = prdist.uniform_prior(reg_model_names)
                    else:
                        # second (or larger) replicate. Use previous posterior as prior
                        prior_dict = post_dict[tar_name]

                    tar_dict = lem_model_dict[rep_idx][tar_name]
                    post_dict[tar_name] = utls.normalize_tar_posterior(tar_dict, prior_dict, beta=inv_temp)
                    tar_dir = ''.join(co['output_dir']) + '/targets'
                    tar_out_df = utls.output_target_file(rep_idx,
                                                         tar_name,
                                                         tar_dict,
                                                         post_dict[tar_name],
                                                         prior_dict,
                                                         tar_dir,
                                                         co)

                    # populate all_scores data frame
                    null_loss = tar_out_df.loc['null_model', 'loss']
                    tar_out_df = tar_out_df.loc[tar_out_df.model != 'null_model']
                    tar_out_df = tar_out_df.loc[:, ['model', 'pld', 'loss']]
                    tar_out_df['norm_loss'] = tar_out_df['loss'] / null_loss
                    tar_out_df['model'] = tar_out_df['model'].apply(lambda x: tar_name + '=' + x)
                    all_scores_df = all_scores_df.append(tar_out_df, ignore_index=True)

                    # write the local minima data to the targets folder
                    minima_df = pd.DataFrame()
                    i=0
                    reg_models = tar_dict.keys()
                    for reg_model in reg_models:
                        # loop over all the local minima found during global optimization
                        for j in range(tar_dict[reg_model].minima_rhs_params.shape[0]):
                            minima_df.loc[i, 'model'] = tar_name + '=' + reg_model
                            minima_df.loc[i, 'loss'] = tar_dict[reg_model].minima_rhs_loss[j]
                            for param_i in range(len(tar_dict[reg_model].minima_rhs_params[j,:])):
                                minima_df.loc[i, 'p%d' % param_i] = tar_dict[reg_model].minima_rhs_params[j, param_i]
                            i=i+1

                    out_file = tar_dir + '/ts' + str(rep_idx) + '/localmin_' + tar_name + '_ts' + str(rep_idx) + '.tsv'
                    with open(out_file, 'w') as out_file:
                        out_file.write("# LEMpy local minima file for target %s, replicate %d\n" % (tar_name, rep_idx))
                        out_file.write("# Right-hand side of tf_act():  p0 - p1(tar) + p2(reg^p4) / (p3^p4 + reg^p4)\n")
                        out_file.write("# Right-hand side of tf_rep():  p0 - p1(tar) + p2(p3^p4) / (p3^p4 + reg^p4)\n")
                        out_file.write("# Run configuration file:  %s\n" % co['config_file'])
                        out_file.write("# Time series data file:   %s\n" % co['data_files'][rep_idx])
                        out_file.write("# Computed on:  %s \n \n" % now.strftime('%Y-%m-%d %H:%M:%S'))
                        minima_df.to_csv(out_file, sep='\t', index=False)


                # if a ground truth edge list is supplied, flag edges in the all scores file
                if co.get('ground_truth_file'):
                    ground_truth_df = pd.read_csv(co['ground_truth_file'], index_col=0, header=None)
                    ground_truth_df['ground_truth'] = 1
                    all_scores_df = all_scores_df.join(ground_truth_df, on='model')
                    utls.output_roc_plots(all_scores_df, config_file_name, rep_idx, co)
                else:
                    co['ground_truth_file'] = ''

                # create summaries directory if necessary
                if not os.path.exists(co['output_dir'] + '/summaries/ts%d/' % rep_idx):
                    os.makedirs(co['output_dir'] + '/summaries/ts%d/' % rep_idx)

                # write all scores files to output directory
                all_scores_df.sort_values('pld', ascending=False, inplace=True)
                with open(co['output_dir'] + '/summaries/ts%s/allscores_ts%s.tsv' % (rep_idx, rep_idx), 'w') as out_file:
                    out_file.write("# LEMpy all scores file for replicate %d \n" % rep_idx)
                    out_file.write("# Run configuration file:  %s \n" % config_file)
                    out_file.write("# Time series data file:   %s \n" % co['data_files'][rep_idx])
                    out_file.write("# Ground truth edge list:  %s \n" % co['ground_truth_file'])
                    out_file.write("# Computed on:  %s \n \n" % now.strftime('%Y-%m-%d %H:%M:%S'))
                    all_scores_df.to_csv(out_file, sep='\t', index=False)




"""
:author: Francis C. Motta
:email: motta<at>math<dot>duke<dot>edu
:created: 11/21/2017
:updated: 11/21/2017
:copyright: (c) 2017, Francis C. Motta

classes and routines for computing generalization loss to empirically determine optimal choice of inverse temperature
"""
from lempy import *


def pair_gen_loss(tar_dict_rep_0, tar_dict_rep_1):
    """
    computes the (directional) generalization loss between replicates for a given target by applying the optimal model 
    determined for replicate 0 to the data in replicate 1
    :param tar_dict_rep_0: dictionary with string keys specifying models of regulation and values equal to localModel 
                           objects for a fixed target, for one replicate
    :param tar_dict_rep_1: dictionary with string keys specifying models of regulation and values equal to localModel 
                           objects for a fixed target, for one replicate
    :return: dictionary with string keys specifying models of regulation and values equal to generalization loss
    """
    gen_loss_dict = {}
    gen_curves_dict = {}
    for reg_model_name, reg_model_rep_0 in tar_dict_rep_0.items():
        gen_loss_dict[reg_model_name] = reg_model_rep_0.loss(reg_model_rep_0.optimal_rhs_params,
                                                             tar_dict_rep_1[reg_model_name].time_working,
                                                             tar_dict_rep_1[reg_model_name].reg_working,
                                                             tar_dict_rep_1[reg_model_name].tar_working,
                                                             reg_model_rep_0.rhs,
                                                             return_pred=False)

        gen_curves_dict[reg_model_name] = reg_model_rep_0.loss(reg_model_rep_0.optimal_rhs_params,
                                                               tar_dict_rep_1[reg_model_name].time_working,
                                                               tar_dict_rep_1[reg_model_name].reg_working,
                                                               tar_dict_rep_1[reg_model_name].tar_working,
                                                               reg_model_rep_0.rhs,
                                                               return_pred=True)

    return gen_loss_dict, gen_curves_dict


def exp_pair_gen_loss(tar_dict_rep_0, tar_dict_rep_1, beta=1.0):
    """
    computes the expected generalization loss between replicates for a given target by applying the optimal model 
    determined for replicate i to the data in replicate j, for each i, j = 1, 2, for a fixed inverse temperature
    :param tar_dict_rep_0: dictionary with string keys specifying models of regulation and values equal to localModel 
                           objects for a fixed target, for one replicate
    :param tar_dict_rep_1: dictionary with string keys specifying models of regulation and values equal to localModel 
                           objects for a fixed target, for one replicate
    :param beta: inverse temperature float used in calculation of posterior, default = 1.0
    :return: the average of the two (bidirectional) expected generalization losses
    """
    # compute probability distributions on local model spaces for each replicate
    post_dict_rep_0 = normalize_tar_posterior(tar_dict_rep_0, beta=beta)
    post_dict_rep_1 = normalize_tar_posterior(tar_dict_rep_1, beta=beta)

    # apply models in replicate 0(1) to data in replicate 1(0) to compute model generalization losses
    gen_loss_dict_rep_01, _ = pair_gen_loss(tar_dict_rep_0, tar_dict_rep_1)
    gen_loss_dict_rep_10, _ = pair_gen_loss(tar_dict_rep_1, tar_dict_rep_0)

    # compute the expected generalization loss in both directions and average
    exp_gen_loss_rep_01 = mp.fsum([mp.fmul(post_dict_rep_0[reg_model_name], gen_loss_dict_rep_01[reg_model_name])
                               for reg_model_name, _ in tar_dict_rep_0.items()])
    exp_gen_loss_rep_10 = mp.fsum([mp.fmul(post_dict_rep_1[reg_model_name], gen_loss_dict_rep_10[reg_model_name])
                               for reg_model_name, _ in tar_dict_rep_0.items()])

    return mp.fdiv(mp.fadd(exp_gen_loss_rep_01, exp_gen_loss_rep_10), mp.mpf(2))


def exp_gen_loss(lem_model_dict, beta=1.0):
    """
    computes the expected generalization loss between replicates for a given target by applying the optimal model 
    determined for replicate i to the data in replicate j, for each i, j, for a fixed inverse temperature
    :param lem_model_dict: nested dictionary of all replicates and all targets containing regulatory model dictionaries 
                           whose string keys encode regulation models and whose values are equal to the corresponding 
                           localModel objects
    :param beta: inverse temperature float used in calculation of posterior, default = 1.0
    :return: 
    """
    # initialize dictionary with keys equal to target names and values equal to empty lists
    rep_inds = list(lem_model_dict.keys())
    rep_pairs = list(itr.combinations(rep_inds, 2))
    tar_names = list(lem_model_dict[rep_inds[0]].keys())
    exp_tar_gen_loss = {tar_name: [] for tar_name in tar_names}

    # compare all pairs of replicates
    for rep_pair in rep_pairs:
        # determine the expected pairwise generalization loss for each target
        for tar_name in tar_names:
            exp_tar_gen_loss[tar_name].append(exp_pair_gen_loss(lem_model_dict[rep_pair[0]][tar_name],
                                                                lem_model_dict[rep_pair[1]][tar_name],
                                                                beta=beta))

    # return the average expected pairwise generalization loss for each target across all replicate pairs
    return {tar_name: mp.fdiv(mp.fsum(exp_tar_gen_loss[tar_name]), mp.mpf(len(rep_pairs))) for tar_name in tar_names}


def output_exp_gen_loss(lem_model_dict, betas, out_dir):

    if not os.path.exists(out_dir + '/x_val'):
        os.makedirs(out_dir + '/x_val')
    out_file = out_dir + '/x_val/x_val.tsv'
    tar_dir = out_dir + '/tar_files'
    orig_tar_dir = out_dir + '/tar_files_beta1'


    for rep_idx in lem_model_dict.keys():
        for tar_name in list(lem_model_dict[rep_idx].keys()):
            posterior_df = pd.DataFrame()
            for beta in betas:
                temp_posterior = normalize_tar_posterior(lem_model_dict[rep_idx][tar_name], beta=beta)
                for reg_model_name in list(temp_posterior.keys()):
                    posterior_df.loc[beta, reg_model_name] = temp_posterior[reg_model_name]
            posterior_df.to_csv(out_dir + '/x_val/posterior_rep_' + str(rep_idx) + '_tar_' + tar_name + '.tsv', sep='\t')


    # output loss and generalization loss results
    for tar_name in list(lem_model_dict[0].keys()):
        # across replicate calculations
        gen_loss_01_df = pd.DataFrame() # generalization loss rep. 0 model & rep. 1 data
        gen_curves_01_df = pd.DataFrame() # predicted curve rep. 0 to rep. 1
        gen_loss_10_df = pd.DataFrame() # generalization loss rep. 1 model & rep. 0 data
        gen_curves_10_df = pd.DataFrame() # predicted curve rep. 1 to rep. 0

        reg_model_names = list(lem_model_dict[0][tar_name].keys())
        gen_curves_01_df['time'] = pd.Series(lem_model_dict[1][tar_name][reg_model_names[0]].time_working).values
        gen_curves_01_df[tar_name] = pd.Series(lem_model_dict[1][tar_name][reg_model_names[0]].tar_working).values
        gen_curves_10_df['time'] = pd.Series(lem_model_dict[0][tar_name][reg_model_names[0]].time_working).values
        gen_curves_10_df[tar_name] = pd.Series(lem_model_dict[0][tar_name][reg_model_names[0]].tar_working).values

        temp_gen_loss_01, temp_gen_curves_01 = pair_gen_loss(lem_model_dict[0][tar_name], lem_model_dict[1][tar_name])
        temp_gen_loss_10, temp_gen_curves_10 = pair_gen_loss(lem_model_dict[1][tar_name], lem_model_dict[0][tar_name])

        for reg_model_name in list(temp_gen_loss_01.keys()):
            gen_loss_01_df[reg_model_name] = pd.Series(temp_gen_loss_01[reg_model_name]).values
            gen_curves_01_df[reg_model_name] = pd.Series(temp_gen_curves_01[reg_model_name]).values
            gen_loss_10_df[reg_model_name] = pd.Series(temp_gen_loss_10[reg_model_name]).values
            gen_curves_10_df[reg_model_name] = pd.Series(temp_gen_curves_10[reg_model_name]).values

        gen_loss_01_df.to_csv(out_dir + '/x_val/gen_loss_01_tar_' + tar_name + '.tsv', sep='\t')
        gen_curves_01_df.to_csv(out_dir + '/x_val/gen_curves_01_tar_' + tar_name + '.tsv', sep='\t')
        gen_loss_10_df.to_csv(out_dir + '/x_val/gen_loss_10_tar_' + tar_name + '.tsv', sep='\t')
        gen_curves_10_df.to_csv(out_dir + '/x_val/gen_curves_10_tar_' + tar_name + '.tsv', sep='\t')

        # within replicate calculations
        reg_loss_0_df = pd.DataFrame() # loss rep. 0 model & rep. 0 data
        reg_curves_0_df = pd.DataFrame()  # rep. 0 predicted curve
        reg_loss_1_df = pd.DataFrame() # loss rep. 1 model & rep. 1 data
        reg_curves_1_df = pd.DataFrame()  # rep. 1 predicted curve

        reg_model_names = list(lem_model_dict[0][tar_name].keys())
        reg_curves_0_df['time'] = pd.Series(lem_model_dict[0][tar_name][reg_model_names[0]].time_working).values
        reg_curves_0_df[tar_name] = pd.Series(lem_model_dict[0][tar_name][reg_model_names[0]].tar_working).values
        reg_curves_1_df['time'] = pd.Series(lem_model_dict[1][tar_name][reg_model_names[0]].time_working).values
        reg_curves_1_df[tar_name] = pd.Series(lem_model_dict[1][tar_name][reg_model_names[0]].tar_working).values

        temp_reg_loss_0, temp_reg_curves_0 = pair_gen_loss(lem_model_dict[0][tar_name], lem_model_dict[0][tar_name])
        temp_reg_loss_1, temp_reg_curves_1 = pair_gen_loss(lem_model_dict[1][tar_name], lem_model_dict[1][tar_name])

        for reg_model_name in list(temp_reg_loss_0.keys()):
            reg_loss_0_df[reg_model_name] = pd.Series(temp_reg_loss_0[reg_model_name]).values
            reg_curves_0_df[reg_model_name] = pd.Series(temp_reg_curves_0[reg_model_name]).values
            reg_loss_1_df[reg_model_name] = pd.Series(temp_reg_loss_1[reg_model_name]).values
            reg_curves_1_df[reg_model_name] = pd.Series(temp_reg_curves_1[reg_model_name]).values

        reg_loss_0_df.to_csv(out_dir + '/x_val/reg_loss_0_tar_' + tar_name + '.tsv', sep='\t')
        reg_curves_0_df.to_csv(out_dir + '/x_val/reg_curves_0_tar_' + tar_name + '.tsv', sep='\t')
        reg_loss_1_df.to_csv(out_dir + '/x_val/reg_loss_1_tar_' + tar_name + '.tsv', sep='\t')
        reg_curves_1_df.to_csv(out_dir + '/x_val/reg_curves_1_tar_' + tar_name + '.tsv', sep='\t')


    exp_gen_loss_df = pd.DataFrame()
    for beta in betas:
        temp_exp_gen_loss = exp_gen_loss(lem_model_dict, beta=beta)
        for tar_name in temp_exp_gen_loss.keys():
            exp_gen_loss_df.loc[beta, tar_name] = temp_exp_gen_loss[tar_name]

    tar_names = list(exp_gen_loss_df.columns.values)

    min_betas = exp_gen_loss_df.idxmin()

    for rep_idx in lem_model_dict.keys():
        j = 0
        for tar_name in tar_names:
            tar_dict = lem_model_dict[rep_idx][tar_name]
            temp_tar_post_dict = normalize_tar_posterior(tar_dict, beta=min_betas[j])
            output_target_file(rep_idx, tar_name, tar_dict, temp_tar_post_dict, tar_dir)
            j=j+1

    for rep_idx in lem_model_dict.keys():
        j = 0
        for tar_name in tar_names:
            tar_dict = lem_model_dict[rep_idx][tar_name]
            temp_tar_post_dict = normalize_tar_posterior(tar_dict, beta=1.0)
            output_target_file(rep_idx, tar_name, tar_dict, temp_tar_post_dict, orig_tar_dir)
            j=j+1

    exp_gen_loss_df.to_csv(out_file, sep='\t')
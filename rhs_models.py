"""
:author: Francis C. Motta
:email: motta<at>math<dot>duke<dot>edu
:created: 04/03/2017
:updated: 12/04/2017
:copyright: (c) 2017, Francis C. Motta

functions for computing right-hand side (rhs) expressions of ode models of single-edge, regulator-target interactions,
i.e. functions, rhs, of the form d(tar)/dt = rhs(reg, tar, params), and rhs parameter region functions
"""


def tf_act(reg, tar, params):
    """
    defines activation Hill function ode rhs of d(tar)/dt assuming reg is expression data for transcriptional activator
    of tar (i.e. tf protein levels), or reasonable proxy for functional activator (i.e. tf rna levels)
    :param reg: length-T regulator time series
    :param tar: length-T target transcript time series
    :param params: length-5 list of parameters specifying rhs model
    :return: length-T d(tar_rna)/dt time series
    """
    g = params[0]
    b = params[1]
    a = params[2]
    k = params[3]
    n = params[4]
    regn = reg ** n
    return g - b * tar + a * (regn) / (k ** n + regn)


def tf_rep(reg, tar, params):
    """
    defines activation Hill function ode rhs of d(tar)/dt assuming reg is expression data for transcriptional repressor
    of tar (i.e. tf protein levels), or reasonable proxy for functional repressor (i.e. tf rna levels)
    :param reg: length-T regulator time series
    :param tar: length-T target transcript time series
    :param params: length-5 list of parameters specifying rhs model
    :return: length-T d(tar_rna)/dt time series
    """
    g = params[0]
    b = params[1]
    a = params[2]
    k = params[3]
    n = params[4]
    kn = k ** n
    return g - b * tar + a * kn / (kn + reg ** n)


def tf_param_bounds():
    """
    defines bounds on parameters in tf model rhs which specifies allowable parameter space
    :return: length-5 list of tuples
    """
    x_min = [0.0, 0.0, 0.0, 0.001, 1.0]
    x_max = [0.3, 1.0, 100.0, 100.0, 10.0]
    return [(low, high) for low, high in zip(x_min, x_max)]


def null(reg, tar, params):
    """
    defines null model of regulation ode rhs of d(tar)/dt assuming no regulator of tar
    :param reg: length-T regulation transcript time series (unused in this model)
    :param tar: length-T target transcript time series
    :param params: length-2 list of parameters specifying rhs model
    :return: length-T d(tar_rna)/dt time series
    """
    g = params[0]
    b = params[1]
    return g - b * tar


def null_bounds():
    """
    defines bounds on parameters in null model rhs which specifies allowable parameter space
    :return: length-2 list of tuples
    """
    x_min = [0.0, 0.0]
    x_max = [0.3, 1.0]
    return [(low, high) for low, high in zip(x_min, x_max)]
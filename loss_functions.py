"""
:author: Francis C. Motta
:email: motta<at>math<dot>duke<dot>edu
:created: 04/03/2017
:updated: 05/10/2017
:copyright: (c) 2017, Francis C. Motta

functions for computing error between target expression data, tar, and prediction of target expression
as a function of parameters, x, of a single-edge regulation ode model, rhs 
"""
import utilities as utls


def euc_loss(x, time, reg, tar, rhs, return_pred=False):
    """
    defines the loss function as a function of parameters appearing in the ode model rhs, as described in the LEM paper 
    (https://tinyurl.com/molgo5f)
    :param x: parameters needed to specify model rhs
    :param time: length-T list of times at which time-series values were measured
    :param reg: length-T list of regulator expression values
    :param tar: length-T list of target expression values
    :param rhs: function defining the right-hand side of the ode describing the rate of change of tar: d(tar)/dt = rhs
    :param return_pred: boolean parameter specifying if the model error or the model target expression is returned
    :return: the mean squared difference between target data and model target expression (default) or length-T list of 
             model-predicted target expression values
    """
    # pass regulator and target through rhs of regulator ode with parameters x
    dtar = rhs(reg, tar, x)
    # integrate rhs of ode and determine optimal constant of integration
    tar_pred = utls.anti_deriv(time, dtar)
    # components of real, len(tar)-dimensional unit vector needed to determine optimal constant of integration
    h = 1 / (len(tar) ** 0.5)
    proj = sum((tar - tar_pred) * h)
    c = proj * h
    tar_pred = tar_pred + c
    if return_pred:
        return tar_pred
    else:
        return utls.mse(tar, tar_pred)

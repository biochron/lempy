# LEMpy - The Local Edge Machine in Python

This Python module implements the Local Edge Machine (https://doi.org/10.1186/s13059-016-1076-z) used to infer functional interactions of variables from time series data.

## Getting Started

The following steps will get you a functional copy of the LEMpy module on your local machine.

### 1) Install Prerequisites

LEMpy requires MPI to be installed for parallelization of parameter optimization across local models of regulation, it has been tested and is known to be compatible with the following:

* Interpreter
```
Python 3.6.0
```

* Modules
```
mpi4py 2.0.0
configobj 5.0.6
mpmath 1.0.0
numpy 1.11.3
scipy 1.0.0
pandas 0.22.0
matplotlib 2.2.2
```

* MPI
```
Microsoft (MS-MPI) v8.1.12438.1091
CentOS (MPICH/HYDRA) v3.1.4
MacOS (Open MPI) v1.6.3
```

### 2) Install LEMpy

From gitbash or terminal:
```
$ git clone git@gitlab.com:biochron/lempy.git
```

### 3) Run Example

To ensure functionality on your local machine, a small example network and data set is provided with this repository.  This examples also serves as an introduction to the input, functionality, and output of LEMpy. Please see the **Introduction to LEM** for details on how to run these examples.

## Introduction to LEM

### Configuration File

The LEMpy module takes as input a single configuration file that adheres to the ConfigObj standard (see http://www.voidspace.org.uk/python/configobj.html for details). The required and optional user-defined parameters contained in a valid configuration file are

* **required command-line arguments**
    * **data_files**: list of full directory and filename(s) of .tsv file(s) containing time series expression data (NOTE: a list with a single data file must end with a comma)
    * **output_dir**: full directory in which to save LEMpy output
    * **loss**: name of the Python function defining the loss function used in the Gibbs posterior
    * **param_bounds**: name of the Python function which returns the parameter bounds for local models
    * **[targets]**: section containing a list of target variable names for which to compute posterior distributions (NOTE: variable name must be followed by '=')
    * **[regulator]**: section containing a list of regulator variable names each followed by a list of Python functions specifying the allowed regulatory models (NOTE: a list with a single allowed model must end with a comma)
* **optional configuration keys**
    * **ground_truth_file**: path to the list of regulation edges which are assumed to be true edges
    * **prior**: name of the Python function which returns a dictionary encoding a prior distribution on the space of regulatory models
    * **normalize**: boolean parameter specifying if time series should be normalized (default: True)
    * **verbose**: boolean parameter specifying if output should print to screen (default: False)
    * **inv_temp**: hyperparameter >= 0 controlling the strength of the update to the prior given the data (default: 1.0)
    * **seed**: number specifying the seed for random number generation (Default: time.time)
    * **[minimizer_params]**: section containing optional parameters for scipy.basinhopping

An example configuration file is contained in the /exp/ subfolder. Currently, all section-specified parameters (e.g. [targets], [regulators], etc.) must appear at the end of the configuration file, i.e. after all individual parameters (e.g. loss, verbose, etc.).

### Data Input

LEMpy expects the data file(s) to be tab-delimited files containing time series data for all target and regulator variables, formatted as follows:

| time | 0 | 4 | ... | 196 |
| :---: | :---: | :---: | :---: | :---: |
| A | 653.13 | 626.35 | ... | 674.12 | 
| B | 618.55 | 618.71 | ... | 624.77 |
| C | 337.00 | 345.22 | ... | 329.48 |

The first column must contain strings specifying variable names and the first row denotes the *time* variable. The first row records measurement times and all other rows contain the time series for target and regulator variables.  Multiple data files may optionally be supplied as input (for example if replicate time series data has been collected for a given set of variables). **Note:** Because a LEMpy configuration file specifies target and regulator variables, the input data file(s) may contain unused variables and time series.

LEMpy optionally supports inclusion of a ground truth edge list file. This file should contain a list of known models of regulation in the format `target=model_handle(regulators)`, where `model_handle` is the name of python function defining the model of regulation of the rate of change of the `target` variable. 

### Functionality

LEM computes a posterior probability distribution on a set of models of regulation $`dX/dt = F_i(\cdot)`$, of a target variable $`X(t)`$. $`F_i`$, $`i=1, \ldots, N`$ define the space of parameterized candidate models of regulation using time series data for the target ($`X`$) and the potential regulators (input variables to the $`F_i`$).  This posterior may be regarded as an update to a prior belief about local models of regulation of a target (specified by a prior distribution) given the observed data. If multiple time series data files are given, LEMpy automatically uses the posterior distribution computed for time series $`k-1`$ as the prior distribution for time series $`k`$.  

### running the example

**3-node repressilator**

The example time series in this data set was generated *in silico* by the system of ODEs

$`\dfrac{dA}{dt} = \gamma_A - \beta_A A + \alpha_A \dfrac{{K_A}^{n_A}}{{K_A}^{n_A} + {C}^{n_A}}`$

$`\dfrac{dB}{dt} = \gamma_B - \beta_B B + \alpha_B \dfrac{{K_B}^{n_B}}{{K_B}^{n_B} + {A}^{n_B}}`$

$`\dfrac{dC}{dt} = \gamma_C - \beta_C C + \alpha_C \dfrac{{K_C}^{n_C}}{{K_C}^{n_C} + {B}^{n_C}}`$

representing the transcriptional repressilator

![synnet_3](exp/synnet_3/synnet_3.png)

From inside the lempy root directory, the following command can be used to execute a parallelized LEMpy run
```
$ mpiexec -n <number of processes> python lempy.py <path to config file> <optional parameters>
```
where the optional parameters may be used to update or add parameters to the specified configuration file.

In particular, the command
```
$ mpiexec -n 4 python lempy.py exp/synnet_3_config.txt
```
will use 4 processes to compute the posterior probability distributions over the set of 6 single-edge regulations (transcriptional activation or repression) of each of the three variables in the repressilator network.

### Output

The output of LEMpy will be a directory specified in the configuration file parameter *output_dir* which contains several subdirectories and files:
```
output_dir/
└─── config_file
└─── summaries/
│    └─── ts0/
│    │    └─── all_scores_ts0.tsv
│    │    └─── (optional) rocplot_ts0.pdf
│    └─── ts1/
│         └─── all_scores_ts1.tsv
│         └─── (optional) rocplot_ts1.pdf
└─── targets/
     └─── ts0/
     │    └─── target_var1_ts0.tsv
     │    └─── target_var2_ts0.tsv
     │    └─── localmin_var1_ts0.tsv
     │    └─── ...
     └─── ts1/
     │    └─── var1_ts1.tsv
     │    └─── var2_ts1.tsv
     │    └─── ...
     └─── ...
```

The subfolder /targets/tsM/ corresponds to the M-th time series data file (e.g. M-th replicate) in the list specified in the configuration file parameter *data_files*.

Each target's output file (target_varN_tsM.tsv) contained in the subfolders /targets/tsM/ corresponds to the target specified by the file name, and contains one row for each of the considered local models of regulation of that target. Each target file contains the following columns
* **model**: string specifying the model of regulation
* **loss**: value of the loss function at the global optimum.
* **pld**: computed posterior probability of the model of regulation
* **inv_temp**: hyperparameter >= 0 controlling the strength of the update to the prior given the data.
* **prior**: assumed prior probability of the model of regulation.
* **rhs_param_i**: The value of the i-th parameter at the global optimum.
* **hess_eigs**: List of eigenvalues of the Hessian of the loss function at the global optimum.
* **hess_det**: Determinant of the Hessian of the loss function at the global optimum.
* **bnd_params**: Number of parameters on the boundary of the allowed parameter region.
* **pld_time**: Time taken to compute the Hessian and the pld score at the global optimum.
* **opt_time**: Time taken to find the global optimum.

Also included in each target file is the loss associated with the `null` model, which corresponds to the target being unregulated. 

Each target's local minima file (localmin_varN_tsM.tsv) contained in the subfolders /targets/tsM/ contains the local minima found during the global optimization of each local model of regulation for that target. There is one row for each local minimum found and each row contains the parameter value and the loss value at that parameter.

The subfolder /summaries/tsM/ corresponds to the M-th time series data file (e.g. M-th replicate) in the list specified in the configuration file parameter *data_files*. The file `allscores_tsM.tsv` contains the columns **pld**, **loss**, and **norm_loss**, where the latter is the **loss** value of the given model of regulation divided by the loss achieved by the `null_model` of regulation of the given target. The column **ground_truth** is included and flags those models of regulation listed as true edges given in the optional **ground_truth_file**.


## Authors

* **Francis Motta**
* **Anastasia Deckard**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

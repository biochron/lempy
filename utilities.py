"""
:author: Francis C. Motta
:email: motta<at>math<dot>duke<dot>edu
:created: 06/25/2017
:updated: 06/25/2017
:copyright: (c) 2017, Francis C. Motta

utility functions supporting other LEMpy routines
"""
import os
import datetime
import numpy as np
import mpmath as mp
import pandas as pd
import random as rng
import rhs_models as rhsm
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc


def config_file_functions(config_obj, fxns=None):
    """
    identify the python functions specified in the LEMpy configuration file to check for compatibility 
    :param config_obj: ConfigObj object 
    :param fxns: a list of string names of functions
    :return: list of unique string names of functions specified in config file
    """
    # list of optional configuration keys which shouldn't contain callable types
    ignore_keys = {'data_files', 'output_dir', 'verbose', 'inv_temp', 'targets', 'normalize', 'disp', 'T',
                   'stepsize', 'interval', 'niter', 'niter_success', 'seed', 'ground_truth_file', 'config_file',
                   'annotation_file', 'gene_list_file', 'num_proc', 'edge_score_file'}
    if fxns is None:
        fxns = []
    for key in config_obj.keys():
        # keys with values equal to dictionaries whose values may contain python function handles
        if key == 'regulators' or key == 'minimizer_params':
            fxns = config_file_functions(config_obj[key], fxns)
        # keys for which to ignore values as they should not contain python function handles
        elif key not in ignore_keys:
            # append python function handles
            if isinstance(config_obj[key], list):
                fxns = fxns + [fxn for fxn in config_obj[key]]
            else:
                fxns.append(config_obj[key])
    return list(set([fxn for fxn in fxns if fxn != '']))


def str_to_bool(string):
    if string.lower() in ('true', 'yes', 't', '1'):
        return True
    elif string.lower() in ('false', 'no', 'n', '0'):
        return False
    else:
        raise ValueError


def str_to_type(string, fxn_dict):
    try:
        return int(string)
    except:
        try:
            return float(string)
        except:
            try:
                return str_to_bool(string)
            except:
                try:
                    return fxn_dict[string]
                except:
                    return string


def anti_deriv(x, dy):
    """
    estimates numerical antiderivative y(x) of function dy/dx(x)
    :param x: length-T list representing domain of dy/dx
    :param dy: length-T list representing dy/dx
    :return: length-(T-1) list estimating y(x)
    """
    n = len(x)
    dx = x[1:]-x[:-1]
    y = 0.5 * (dy[1:] + dy[:-1]) * dx
    y = par_sum(y)
    return y


def par_sum(vec):
    """
    computes the partial sums of a list of numbers
    :param vec: length-(T-1) list of numbers
    :return: length(T) list of partial (cumulative) sums of vec
    """
    return np.concatenate(([0], np.cumsum(vec)))


def mse(data, pred):
    """
    returns the mean squared error between two lists of numbers of equal length
    :param data: length-T list of numbers
    :param pred: length-T list of numbers
    :return: average squared difference between components of data and pred
    """
    return sum((data - pred) ** 2) / len(data)


def second_deriv_tuple(dim, i, j):
    """
    creates a second derivative index tuple
    :param dim: dimension of domain of function being differentiated
    :param i: index of variables with which to take one derivative
    :param j: index of variables with which to take one derivative
    :return: tuple of integers specifying order of derivative in each variable
    """
    derivs = [0 for _ in range(dim)]
    derivs[i] += 1
    derivs[j] += 1
    return tuple(derivs)


def bounded_accept_test(**kwargs):
    """
    modifies the stepper in scipy.basinhopping to only accept proposals which are within the allowable parameter space
    """
    bounds = rhsm.tf_param_bounds()
    x_min = [p[0] for p in bounds]
    x_max = [p[1] for p in bounds]
    x = kwargs["x_new"]
    t_max = bool(all([x[i] <= x_max[i] for i in range(len(x))]))
    t_min = bool(all([x[i] >= x_min[i] for i in range(len(x))]))
    return t_max and t_min


class bounded_take_step(object):
    """
    modifies the stepper in scipy.basinhopping so that maximum displacement of each coordinate is proportional to the 
    width of allowable parameter space in that coordinate
    """

    def __init__(self, stepsize=0.5):
        self.stepsize = stepsize
        bounds = rhsm.tf_param_bounds()
        bounds_widths = [p[1] - p[0] for p in bounds]
        max_width = max(bounds_widths)
        self.bounds_props = [w / max_width for w in bounds_widths]

    def __call__(self, x_old):
        s = self.stepsize
        p = self.bounds_props
        x_new = [x_old[i] + rng.uniform(-s * p[i], s * p[i]) for i in range(len(x_old))]
        return x_new


def normalize_tar_posterior(tar_dict, prior_dict, beta=1.0):
    """
    normalize the pld scores for models of regulation of a fixed target to compute posterior probability distribution
    :param tar_dict: dictionary with string keys specifying models of regulation and values equal to localModel
                     objects for a fixed target
    :param prior_dict: dictionary with string keys specifying models of regulation and values equal to prior
                       probabilities
    :param beta: inverse temperature float used in calculation of posterior, default = 1.0
    :return: dictionary mapping of models of regulation of target to (normalized) posterior probabilities
             i.e. the posterior probability distribution on the space of regulatory models
    """
    # compute unnormalized probabilities for each regulation model in tar_dict
    posterior_dict = {}
    for reg_model_name, reg_model in tar_dict.items():
        reg_model.pld(beta=beta)
        posterior_dict[reg_model_name] = reg_model.posterior * prior_dict[reg_model_name]
    # normalize probabilities to compute posterior distribution
    norm_factor = mp.fsum(posterior_dict.values())
    return {reg_model_name: mp.fdiv(prob, norm_factor) for reg_model_name, prob in posterior_dict.items()}


def output_target_file(rep_idx, tar_name, tar_dict, post_dict, prior_dict, tar_dir, config_obj):
    """
    write a tab-delimited target file containing details about the local models of regulation of the target
    :param rep_idx: integer specifying the replicate
    :param tar_name: string specifying the current target
    :param tar_dict: dictionary whose string keys encode regulation models and whose values are equal to the
                     corresponding localModel objects
    :param post_dict: dictionary containing normalized posterior distribution for current target
    :param prior_dict: dictionary with string keys specifying models of regulation and values equal to prior
                       probabilities
    :param tar_dir: string full path to base targets output directory
    :param config_obj: configuration file object
    :return out_df: the output target dataframe
    """
    now = datetime.datetime.now()

    # create replicate directory and specify output file
    if not os.path.exists(tar_dir + '/ts' + str(rep_idx)):
        os.makedirs(tar_dir + '/ts' + str(rep_idx))
    out_file = tar_dir + '/ts' + str(rep_idx) + '/target_' + tar_name + '_ts' + str(rep_idx) + '.tsv'

    # construct DataFrame for export
    reg_models = tar_dict.keys()
    out_df = pd.DataFrame(index=reg_models)
    for reg_model in reg_models:
        out_df.loc[reg_model, 'model'] = reg_model
        out_df.loc[reg_model, 'loss'] = tar_dict[reg_model].optimal_rhs_loss
        out_df.loc[reg_model, 'pld'] = mp.nstr(post_dict[reg_model],
                                               min_fixed=-mp.inf,
                                               max_fixed=mp.inf)
        out_df.loc[reg_model, 'inv_temp'] = tar_dict[reg_model].beta
        out_df.loc[reg_model, 'prior'] = prior_dict[reg_model]
        for param_i in range(len(tar_dict[reg_model].optimal_rhs_params)):
            out_df.loc[reg_model, 'rhs_param_' + str(param_i)] = tar_dict[reg_model].optimal_rhs_params[param_i]
        out_df.loc[reg_model, 'hess_eigs'] = mp.nstr(tar_dict[reg_model].hessian_eigs,
                                                     min_fixed=-mp.inf,
                                                     max_fixed=mp.inf)
        out_df.loc[reg_model, 'hess_det'] = tar_dict[reg_model].hessian_det
        out_df.loc[reg_model, 'bnd_params'] = tar_dict[reg_model].params_on_boundary
        out_df.loc[reg_model, 'pld_time'] = tar_dict[reg_model].pld_time
        out_df.loc[reg_model, 'opt_time'] = tar_dict[reg_model].opt_time

    # compute row for null model
    null_x, null_loss = tar_dict[list(reg_models)[0]].compute_null_loss()
    out_df.loc['null_model', 'model'] = 'null_model'
    out_df.loc['null_model', 'loss'] = null_loss
    out_df.loc['null_model', 'rhs_param_0'] = null_x[0]
    out_df.loc['null_model', 'rhs_param_1'] = null_x[1]

    # sort and write target file
    out_df.sort_values('pld', ascending=False, inplace=True)
    with open(out_file, 'w') as out_file:
        out_file.write("# LEMpy target file for %s, replicate %d \n" % (tar_name, rep_idx))
        out_file.write("# Run configuration file:  %s \n" % config_obj['config_file'])
        out_file.write("# Time series data file:   %s \n" % config_obj['data_files'][rep_idx])
        out_file.write("# Computed on:  %s \n \n" % now.strftime('%Y-%m-%d %H:%M:%S'))
        out_df.to_csv(out_file, sep='\t', index=False)

    return out_df


def output_roc_plots(all_scores_df, config_file_name, rep_idx, config_obj):
    """
    Calculate and generate ROC plots and AUC scores for a single lempy run that includes a ground truth edge list
    :param all_scores_df: data frame containing all pld scores for all targets in a given lempy run and the ground truth
                          edge list column.
    :param config_file_name: file name of the time series data set
    :param rep_idx: integer tracking index of replicate
    :param config_obj: configuration file object
    :return:
    """
    # convert ground truth values to binary and pld scores to floats
    all_scores_df.fillna(value=0, inplace=True)
    all_scores_df['ground_truth'] = all_scores_df['ground_truth'].astype(int)
    all_scores_df['pld'] = all_scores_df['pld'].astype(float)

    # create summaries directory if necessary
    if not os.path.exists(config_obj['output_dir'] + '/summaries/ts%d/' % rep_idx):
        os.makedirs(config_obj['output_dir'] + '/summaries/ts%d/' % rep_idx)

    # compute roc curve and auc score
    fpr, tpr, _ = roc_curve(all_scores_df['ground_truth'].tolist(), all_scores_df['pld'].tolist())
    roc_auc = auc(fpr, tpr)

    # generate and save figure
    f = plt.figure()
    plt.plot(fpr, tpr, color='darkorange', lw=2, label='AUC = %0.2f' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=1, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC plot | data file: %s' % config_file_name)
    plt.legend(loc="lower right")
    f.savefig(config_obj['output_dir'] + '/summaries/ts%d/roccurve_ts%d.pdf' % (rep_idx, rep_idx),
              bbox_inches='tight')

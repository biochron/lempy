"""
:author: Francis C. Motta
:email: motta<at>math<dot>duke<dot>edu
:created: 06/25/2017
:updated: 06/25/2017
:copyright: (c) 2017, Francis C. Motta

functions for generating prior distributions which take as input the list of strings of regulatory model 
names (used to index the space of local regulatory models) and outputs a dictionary string keys encoding 
models and values encoding uniform probability  distribution on space of local models
"""


def uniform_prior(reg_model_names):
    """
    generate a prior distribution on the space of regulatory interactions given by a list of reg_model_names
    :param reg_model_names: 
    :return: dictionary with string keys encoding regulation models and values encoding uniform probability 
             distribution on space of local models of regulation
    """
    num_reg_models = len(reg_model_names)
    return {reg_model: 1 / num_reg_models for reg_model in reg_model_names}
